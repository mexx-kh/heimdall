defmodule Heimdall.Mixfile do
  use Mix.Project

  def project do
    [
      app: :heimdall,
      version: "0.9.1",
      elixir: "~> 1.10",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: act_as_prod?(),
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      mod: {Heimdall.Application, []},
      extra_applications: [:lager, :logger, :elixir_uuid]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp act_as_prod? do
    case Mix.env() do
      :prod -> true
      :staging -> true
      _ -> false
    end
  end

  defp deps do
    [
      {:elixir_uuid, "~> 1.2"},
      {:amqp, "~> 1.5.0"}
    ]
  end

  defp aliases do
    [
      compile: ["compile --warnings-as-errors"]
    ]
  end
end
