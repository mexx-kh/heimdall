use Mix.Config

config :logger, level: :warn

config :lager,
  handlers: [
    lager_console_backend: [level: :error]
  ]
