use Mix.Config

config :heimdall,
  adapter: Heimdall.Adapters.Sandbox

import_config "#{Mix.env()}.exs"
