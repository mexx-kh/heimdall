defmodule SimpleConsumer do
  use Heimdall.Consumer, events: :heimdall_create

  def consume({:ok, %{sender: sender_pid, event: event}}) do
    send(sender_pid, %{event_received: event, from: __MODULE__})
  end
end
