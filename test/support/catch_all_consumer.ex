defmodule CatchAllConsumer do
  use Heimdall.Consumer, events: :all

  def consume({:ok, %{sender: sender_pid, event: event}}) do
    send(sender_pid, %{event_received: event, from: __MODULE__})
  end
end
