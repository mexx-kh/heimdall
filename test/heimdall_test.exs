defmodule HeimdallTest do
  use ExUnit.Case

  alias Heimdall.Adapters.Sandbox.Producer

  setup _ do
    start_supervised({Heimdall.Supervisor, {:otp_app, :heimdall}})
    Producer.enable_sender()
    :ok
  end

  test "event published is consumed by the interested consumer" do
    Heimdall.broadcast_event({:ok, %{event: :heimdall_create, sender: self()}})
    assert_receive(%{event_received: :heimdall_create, from: SimpleConsumer})
  end

  test "event published is ignored by the consumer" do
    Heimdall.broadcast_event({:ok, %{event: :heimdall_update, sender: self()}})
    refute_receive(%{event_received: :heimdall_update, from: SimpleConsumer})
  end

  test "consumer is able to listen all events" do
    Heimdall.broadcast_event({:ok, %{event: :heimdall_read, sender: self()}})
    Heimdall.broadcast_event({:ok, %{event: :heimdall_remove, sender: self()}})

    assert_receive(%{event_received: :heimdall_read, from: CatchAllConsumer})
    assert_receive(%{event_received: :heimdall_remove, from: CatchAllConsumer})
  end
end
