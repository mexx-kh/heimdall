defmodule Heimdall.EventCaseTest do
  use ExUnit.Case
  use Heimdall.EventCase

  alias ExUnit.AssertionError

  describe "assert_broadcast_event/2" do
    test "fails when payload doesn't match" do
      assert_raise(AssertionError, ~r/Event payload didn't match/, fn ->
        assert_broadcast_event(:event_name, payload: %{some: :value, other: :val}) do
          Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value}})
        end
      end)
    end

    test "succeeds when payload matches" do
      assert_broadcast_event :event_name, payload: %{some: :value} do
        Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
      end
    end

    test "succeeds when a non-matching event gets sent after the matching one" do
      assert_broadcast_event :event_name, payload: %{some: :value} do
        Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
        Heimdall.broadcast_event({:ok, %{event: :no_match_event, some: :value, other: :val}})
      end
    end

    test "succeeds when a non-matching event gets sent before the matching one" do
      assert_broadcast_event :event_name, payload: %{some: :value} do
        Heimdall.broadcast_event({:ok, %{event: :no_match_event, some: :value, other: :val}})
        Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
      end
    end

    test "fails when execution block is async and takes more than default timeout" do
      assert_raise(AssertionError, ~r/Expected event event_name/, fn ->
        assert_broadcast_event :event_name, payload: %{some: :value} do
          Task.start_link(fn ->
            Process.sleep(101)
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
          end)
        end
      end)
    end

    test "succeeds when execution block is async and takes less than specified timeout" do
      assert_broadcast_event :event_name, payload: %{some: :value}, timeout: 200 do
        Task.start_link(fn ->
          Process.sleep(101)
          Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
        end)
      end
    end

    test "fails when execution block is async and takes more than specified timeout" do
      assert_raise(AssertionError, ~r/Expected event event_name/, fn ->
        assert_broadcast_event :event_name, payload: %{some: :value}, timeout: 50 do
          Task.start_link(fn ->
            Process.sleep(80)
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
          end)
        end
      end)
    end

    test "fails when payload keys match but the values don't" do
      assert_raise(AssertionError, ~r/Event payload didn't match/, fn ->
        assert_broadcast_event :event_name, payload: %{some: :val} do
          Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
        end
      end)
    end

    test "succeeds when event matches without specifying payload" do
      assert_broadcast_event :event_name do
        Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
      end
    end

    test "fails when event doesn't match" do
      assert_raise(
        AssertionError,
        ~r/Expected event event_name to be broadcasted but it wasn't/,
        fn ->
          assert_broadcast_event :event_name do
            Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
          end
        end
      )
    end

    test "fails when payload matches but event doesn't" do
      assert_raise(
        AssertionError,
        ~r/Expected event event_name to be broadcasted but it wasn't/,
        fn ->
          assert_broadcast_event :event_name, payload: %{some: :value, other: :val} do
            Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
          end
        end
      )
    end
  end

  describe "refute_broadcast_event/2" do
    test "fails when the event is broadcasted" do
      assert_raise(
        AssertionError,
        ~r/Expected event event_name not to be broadcasted but it was/,
        fn ->
          refute_broadcast_event :event_name do
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
          end
        end
      )
    end

    test "succeeds when execution block is async and the event is broadcasted after the default timeout" do
      refute_broadcast_event :event_name do
        Task.start_link(fn ->
          Process.sleep(110)
          Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
        end)
      end
    end

    test "succeeds when execution block is async and the event is broadcasted after the given timeout" do
      refute_broadcast_event :event_name, timeout: 50 do
        Task.start_link(fn ->
          Process.sleep(60)
          Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
        end)
      end
    end

    test "fails when execution block is async and the event is broadcasted before the given timeout" do
      assert_raise(
        AssertionError,
        ~r/Expected event event_name not to be broadcasted but it was/,
        fn ->
          refute_broadcast_event :event_name, timeout: 50 do
            Task.start_link(fn ->
              Process.sleep(40)

              Heimdall.broadcast_event(
                {:ok, %{event: :no_match_event, some: :value, other: :val}}
              )

              Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
            end)
          end
        end
      )
    end

    test "fails when the event is broadcasted after a non matching event" do
      assert_raise(
        AssertionError,
        ~r/Expected event event_name not to be broadcasted but it was/,
        fn ->
          refute_broadcast_event :event_name do
            Heimdall.broadcast_event({:ok, %{event: :no_match_event, some: :value, other: :val}})
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
          end
        end
      )
    end

    test "succeeds when the event is not broadcasted" do
      refute_broadcast_event :event_name do
        Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
      end
    end
  end

  describe "assert_multiple_broadcast_event/1" do
    test "fails when any of the events' payload doesn't match" do
      assert_raise(AssertionError, ~r/Event payload didn't match/, fn ->
        assert_multiple_broadcast_event %{
          event_name: [payload: %{some: :value, other: :val}],
          other_event: [payload: %{wrong: :value}]
        } do
          Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
          Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
        end
      end)
    end

    test "fails when any of the events is not broadcasted" do
      assert_raise(
        AssertionError,
        ~r/Expected event not_broadcasted_event to be broadcasted but it wasn't/,
        fn ->
          assert_multiple_broadcast_event %{
            event_name: [],
            other_event: [],
            not_broadcasted_event: []
          } do
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
            Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
          end
        end
      )
    end

    test "succeeds when all expected events and payloads match" do
      assert_multiple_broadcast_event %{
        event_name: [payload: %{some: :value, other: :val}],
        other_event: [payload: %{some: :value, other: :val}]
      } do
        Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
        Heimdall.broadcast_event({:ok, %{event: :unexpected_event, payload: %{}}})
        Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
      end
    end

    test "fails when execution block is async and the event broadcasting takes more than the given timeout" do
      assert_raise(AssertionError, ~r/Expected event event_name/, fn ->
        assert_multiple_broadcast_event %{
                                          event_name: [payload: %{some: :value, other: :val}],
                                          other_event: [payload: %{some: :value, other: :val}]
                                        },
                                        timeout: 50 do
          Task.start_link(fn ->
            Process.sleep(70)
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
            Heimdall.broadcast_event({:ok, %{event: :unexpected_event, payload: %{}}})
            Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
          end)
        end
      end)
    end

    test "fails when execution block is async and the event broadcasting takes more than the default timeout" do
      assert_raise(AssertionError, ~r/Expected event event_name/, fn ->
        assert_multiple_broadcast_event %{
          event_name: [payload: %{some: :value, other: :val}],
          other_event: [payload: %{some: :value, other: :val}]
        } do
          Task.start_link(fn ->
            Process.sleep(101)
            Heimdall.broadcast_event({:ok, %{event: :event_name, some: :value, other: :val}})
            Heimdall.broadcast_event({:ok, %{event: :unexpected_event, payload: %{}}})
            Heimdall.broadcast_event({:ok, %{event: :other_event, some: :value, other: :val}})
          end)
        end
      end)
    end
  end
end
