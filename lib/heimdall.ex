defmodule Heimdall do
  require Logger

  alias Heimdall.Event

  # Public API
  defdelegate broadcast_event(payload, type \\ :local), to: Event, as: :publish

  def worker_spec(_, :test), do: []

  def worker_spec(otp_app, _) do
    Supervisor.Spec.supervisor(Heimdall.Supervisor, otp_app: otp_app)
  end
end
