defmodule Heimdall.Supervisor do
  use Supervisor

  require Logger
  alias Heimdall.{Helper, Event}

  # Client API

  def start_link({:otp_app, otp_app}) do
    Supervisor.start_link(__MODULE__, otp_app, name: Helper.server_name(otp_app, __MODULE__))
  end

  def init(otp_app) do
    config = Application.get_all_env(:heimdall)

    children = get_consumers(config, otp_app)

    Supervisor.init(children, strategy: :one_for_one)
  end

  # Internal Functions

  defp get_consumers(config, otp_app) do
    {:ok, modules} = :application.get_key(otp_app, :modules)
    config = Keyword.put(config, :otp_app, otp_app)

    modules
    |> Enum.map(fn module ->
      build_consumer_spec(config, module, module.module_info(:attributes)[:behaviour])
    end)
    |> Enum.reject(&is_nil/1)
  end

  def build_consumer_spec(_, _, nil), do: nil

  def build_consumer_spec(config, module, behaviours) do
    if Enum.member?(behaviours, Heimdall.Consumer) do
      %{
        start: {
          Module.concat(config[:adapter], Consumer),
          :start_link,
          [config, module, Event.registered_events(module)]
        },
        id: String.to_atom(to_string(module))
      }
    end
  end
end
