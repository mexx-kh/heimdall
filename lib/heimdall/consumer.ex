defmodule Heimdall.Consumer do
  @callback consume(tuple) :: no_return

  defmacro __using__(opts) do
    quote do
      @behaviour unquote(__MODULE__)
      Module.register_attribute(__MODULE__, :events, persist: true)
      @events Keyword.get(unquote(opts), :events)
    end
  end
end
