defmodule Heimdall.Application do
  use Application

  def start(_type, _args) do
    config = Application.get_all_env(:heimdall)

    children = [
      connection_spec(config, :producer_connection),
      connection_spec(config, :consumer_connection),
      {Module.concat(config[:adapter], Producer), config}
    ]

    opts = [strategy: :one_for_one, name: HeimdallSupervisor]
    Supervisor.start_link(children, opts)
  end

  # Internal functions

  defp connection_spec(config, name) do
    connection = Module.concat(config[:adapter], Connection)
    %{start: {connection, :start_link, [config, name]}, id: name}
  end
end
