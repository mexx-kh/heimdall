defmodule Heimdall.Adapters.Sandbox.Producer do
  use GenServer

  alias Heimdall.Event
  alias Heimdall.Adapters.Sandbox.Consumer

  require Logger

  @server SandboxProducer

  # Client API

  def publish(payload) do
    GenServer.cast(@server, {:publish, payload})
  end

  def subscribe(subscriber) do
    GenServer.cast(@server, {:subscribe, subscriber})
  end

  def enable_sender() do
    GenServer.call(@server, :enable_sender)
  end

  def disable_sender() do
    GenServer.call(@server, :disable_sender)
  end

  def start_link(_args) do
    case GenServer.start_link(__MODULE__, [], name: @server) do
      {:error, {:already_started, pid}} -> {:ok, pid}
      producer -> producer
    end
  end

  # GenServer callbacks

  def init(_args) do
    {:ok, %{subscribers: MapSet.new(), enable_sender: false}}
  end

  def handle_call(:enable_sender, _from, state) do
    {:reply, true, %{state | enable_sender: true}}
  end

  def handle_call(:disable_sender, _from, state) do
    {:reply, true, %{state | enable_sender: false}}
  end

  def handle_cast({:subscribe, subscriber}, %{subscribers: subscribers} = state) do
    {:noreply, %{state | subscribers: MapSet.put(subscribers, subscriber)}}
  end

  def handle_cast({:publish, _payload}, %{enable_sender: false} = state) do
    {:noreply, state}
  end

  def handle_cast({:publish, payload}, %{subscribers: subscribers} = state) do
    subscribers
    |> Enum.each(fn subscriber ->
      subscriber
      |> get_consumers()
      |> send_events(payload)
    end)

    {:noreply, state}
  end

  # Internal Functions

  defp get_consumers(otp_app) do
    {:ok, modules} = :application.get_key(otp_app, :modules)

    modules
    |> Enum.map(fn module ->
      {module, get_events(module, module.module_info(:attributes)[:behaviour])}
    end)
    |> Enum.reject(fn {_, events} -> is_nil(events) end)
  end

  def get_events(_, nil), do: nil

  def get_events(module, behaviours) do
    if Enum.member?(behaviours, Heimdall.Consumer) do
      Event.registered_events(module)
    end
  end

  def send_events(modules, {:ok, %{event: event}} = payload) do
    modules
    |> Enum.filter(fn {_, events} -> match_event?(event, events) end)
    |> Enum.map(fn {module, _} -> Consumer.consume(module, payload) end)
  end

  def match_event?(_, ["#"]), do: true
  def match_event?(event, events), do: event in events
end
