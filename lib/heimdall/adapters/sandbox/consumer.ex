defmodule Heimdall.Adapters.Sandbox.Consumer do
  alias Heimdall.Adapters.Sandbox.Producer

  def consume(module, payload) do
    apply(module, :consume, [payload])
  end

  def start_link(config, _module, _events) do
    :ok = Producer.subscribe(config[:otp_app])
    {:ok, :pid}
  end

  def init(_) do
    {:ok, :no_state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end
end
