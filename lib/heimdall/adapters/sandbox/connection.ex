defmodule Heimdall.Adapters.Sandbox.Connection do
  def start_link(_, _) do
    {:ok, self()}
  end

  def init(_) do
    {:ok, :no_state}
  end
end
