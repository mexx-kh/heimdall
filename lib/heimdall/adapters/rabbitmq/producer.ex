defmodule Heimdall.Adapters.RabbitMQ.Producer do
  use GenServer

  alias Heimdall.Adapters.RabbitMQ
  alias Heimdall.Adapters.RabbitMQ.Connection

  require Logger

  @producer HeimdallProducer

  # Client API

  def publish(message) do
    GenServer.cast(@producer, {:publish, message})
  end

  def start_link(config) do
    GenServer.start_link(__MODULE__, config, name: @producer)
  end

  # GenServer callbacks

  def init(app_config) do
    {:ok, channel} = Connection.get_channel(:producer_connection)
    RabbitMQ.exchange(channel, app_config[:exchange])

    Process.flag(:trap_exit, true)
    ref = Process.monitor(channel.pid)

    {:ok, %{channel: channel, exchange: app_config[:exchange][:name], ref: ref}}
  end

  def handle_cast(
        {:publish, {:ok, %{event: routing_key}} = message},
        %{channel: channel, exchange: exchange} = state
      ) do
    do_publish(message, channel, exchange, routing_key)
    {:noreply, state}
  end

  def handle_cast(
        {:publish, %{event: routing_key, payload: message}},
        %{channel: channel, exchange: exchange} = state
      ) do
    do_publish(message, channel, exchange, routing_key)
    {:noreply, state}
  end

  def handle_cast({:publish, message}, state) do
    Logger.warn("Unkown event format: #{inspect(message)}")
    {:noreply, state}
  end

  def handle_info({:DOWN, ref, :process, _, _}, %{ref: ref} = state) do
    {:stop, :kill, state}
  end

  def handle_info(message, state) do
    Logger.warn("Producer unexpected message #{inspect(message)}")
    {:noreply, state}
  end

  def terminate(reason, %{channel: nil}) do
    {:stop, reason}
  end

  def terminate(reason, %{channel: channel}) do
    RabbitMQ.close_channel(channel)
    {:stop, reason}
  end

  # Internal Functions

  defp do_publish(message, channel, exchange, routing_key) do
    payload = :erlang.term_to_binary(message)
    RabbitMQ.publish(channel, exchange, to_string(routing_key), payload)
  end
end
