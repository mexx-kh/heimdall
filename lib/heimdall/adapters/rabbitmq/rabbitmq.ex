defmodule Heimdall.Adapters.RabbitMQ do
  use AMQP

  @prefetch_count 100
  # 5 secs
  @reconnect_time 5_000
  @queue_config [durable: true, auto_delete: false]
  @exchange_config [durable: true, auto_delete: false]

  require Logger

  def publish(channel, exchange, routing_key, payload) do
    Basic.publish(channel, exchange, routing_key, payload)
  end

  def exchange(channel, exchange, opts \\ @exchange_config) do
    :ok = Exchange.declare(channel, exchange[:name], exchange[:type], opts)
    channel
  end

  def queue(channel, name, exchange, opts \\ @queue_config) do
    {:ok, _} = Queue.declare(channel, name, opts)
    bind_queue(channel, name, exchange, opts[:routing_keys] || [])
    channel
  end

  def bind_queue(channel, name, exchange, []) do
    :ok = Queue.bind(channel, name, exchange)
  end

  def bind_queue(channel, name, exchange, routing_keys) when is_list(routing_keys) do
    for key <- routing_keys do
      :ok = Queue.bind(channel, name, exchange, routing_key: to_string(key))
    end
  end

  def queue_name(module) do
    module
    |> Module.split()
    |> Enum.join("_")
    |> String.downcase()
  end

  def consumer(channel, queue) do
    :ok = Basic.qos(channel, prefetch_count: @prefetch_count)
    :ok = Confirm.select(channel)
    {:ok, _} = Basic.consume(channel, queue)
    channel
  end

  def close_channel(channel) do
    AMQP.Channel.close(channel)
  end

  def channel(conn) do
    Channel.open(conn)
  end

  def connect(url) do
    case Connection.open(url) do
      {:ok, conn} ->
        {:ok, conn}

      {:error, error} ->
        :timer.sleep(@reconnect_time)

        Logger.error(
          "Cannot connect to RabbitMQ:  #{inspect(error)}. Retry in #{@reconnect_time} seconds"
        )

        connect(url)
    end
  end

  def ack(channel, tag) do
    :ok = Basic.ack(channel, tag)
  end

  def nack(channel, tag, requeue?) do
    :ok = Basic.nack(channel, tag, requeue: requeue?)
  end

  def reject(channel, tag, requeue?) do
    :ok = Basic.reject(channel, tag, requeue: requeue?)
  end
end
