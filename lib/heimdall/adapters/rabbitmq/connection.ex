defmodule Heimdall.Adapters.RabbitMQ.Connection do
  use GenServer

  alias Heimdall.Adapters.RabbitMQ

  require Logger

  # Client API

  def get_channel(server) do
    GenServer.call(server, :get_channel)
  end

  def start_link(config, name) do
    GenServer.start_link(__MODULE__, config, name: name)
  end

  # GenServer callbacks

  def init(config) do
    Process.flag(:trap_exit, true)
    credentials = config[:rabbitmq]
    send(self(), :connect)
    {:ok, %{connection: nil, ref: nil, credentials: credentials}}
  end

  def handle_call(:get_channel, _from, %{connection: conn} = state) do
    {:reply, RabbitMQ.channel(conn), state}
  end

  def handle_info(:connect, %{credentials: credentials} = state) do
    {:ok, conn} = RabbitMQ.connect(credentials)
    ref = Process.monitor(conn.pid)
    {:noreply, %{state | connection: conn, ref: ref}}
  end

  def handle_info({:DOWN, ref, :process, _, _}, %{ref: ref} = state) do
    send(self(), {:connect, state.credentials})
    {:noreply, %{state | connection: nil, ref: nil}}
  end

  def handle_info(message, state) do
    Logger.warn("Connection unexpected message #{inspect(message)}")
    {:noreply, state}
  end
end
