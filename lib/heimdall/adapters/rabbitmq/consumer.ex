defmodule Heimdall.Adapters.RabbitMQ.Consumer do
  use GenServer

  alias Heimdall.Adapters.RabbitMQ
  alias Heimdall.Adapters.RabbitMQ.Connection

  require Logger

  def start_link(config, module, events) do
    GenServer.start_link(__MODULE__, [config, module, events])
  end

  def init([config, module, events]) do
    exchange = config[:exchange]
    queue = config[:queue]
    queue_name = RabbitMQ.queue_name(module)
    {:ok, channel} = Connection.get_channel(:consumer_connection)

    queue_opts =
      queue
      |> Keyword.get(:opts, [])
      |> Keyword.put(:routing_keys, events)

    channel
    |> RabbitMQ.exchange(exchange)
    |> RabbitMQ.queue(queue_name, exchange[:name], queue_opts)
    |> RabbitMQ.consumer(queue_name)

    Process.flag(:trap_exit, true)
    ref = Process.monitor(channel.pid)

    {:ok, %{channel: channel, handler: module, ref: ref}}
  end

  def handle_info({:basic_consume_ok, _}, state) do
    {:noreply, state}
  end

  def handle_info({:basic_cancel, _}, state) do
    {:stop, :normal, state}
  end

  def handle_info({:basic_cancel_ok, _}, state) do
    {:noreply, state}
  end

  def handle_info(
        {:basic_deliver, message, %{delivery_tag: tag, redelivered: redelivered}},
        %{handler: handler, channel: channel} = state
      ) do
    Task.start(fn ->
      consume_and_acknowledge(channel, tag, redelivered, message, handler)
    end)

    {:noreply, state}
  end

  def handle_info({:DOWN, ref, :process, _, _}, %{ref: ref} = state) do
    {:stop, :kill, state}
  end

  def handle_info(msg, state) do
    Logger.warn("Unhandled message #{inspect(msg)}")
    {:noreply, state}
  end

  # Internal Functions

  defp consume_and_acknowledge(channel, tag, redelivered, message, handler) do
    try do
      payload = :erlang.binary_to_term(message)
      apply(handler, :consume, [payload])
      RabbitMQ.ack(channel, tag)
    rescue
      error ->
        RabbitMQ.nack(channel, tag, not redelivered)
        Logger.error("Error processing event: #{inspect(message)} - #{inspect(error)}")
    end
  end
end
