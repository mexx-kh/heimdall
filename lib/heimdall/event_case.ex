defmodule Heimdall.EventCase do
  use ExUnit.CaseTemplate

  using opts do
    quote do
      import Heimdall.EventCase

      @default_timeout 100

      Module.register_attribute(__MODULE__, :otp_app, persist: true)
      Module.put_attribute(__MODULE__, :otp_app, Keyword.get(unquote(opts), :otp_app, ""))

      Module.register_attribute(__MODULE__, :consume, persist: true)
      Module.put_attribute(__MODULE__, :otp_app, Keyword.get(unquote(opts), :consume, false))

      setup do
        otp_app = Keyword.get(unquote(opts), :otp_app, nil)
        enable_sender = Keyword.get(unquote(opts), :enable_sender, false)

        start_supervised({Heimdall.Supervisor, {:otp_app, otp_app}})

        if enable_sender do
          true = Heimdall.Adapters.Sandbox.Producer.enable_sender()
        end

        :gen_event.delete_handler(:lager_event, :lager_console_backend, [])

        :ok
      end

      defp assert_event_receive(event, options \\ []) do
        timeout = Keyword.get(options, :timeout, @default_timeout)
        pid = Process.whereis(SandboxProducer)

        expected_payload = Keyword.get(options, :payload, %{})

        received_payload = assert_event_broadcast(pid, event, timeout)
        assert_event_payload_match(expected_payload, received_payload)

        received_payload
      end

      defp assert_event_broadcast(pid, event, timeout) do
        assert_receive(
          {:trace, ^pid, :receive,
           {:"$gen_cast", {:publish, {:ok, %{event: ^event} = received_payload}}}},
          timeout,
          "Expected event #{event} to be broadcasted but it wasn't"
        )

        received_payload
      end

      defp assert_event_payload_match(expected, actual) do
        assert MapSet.subset?(MapSet.new(expected), MapSet.new(actual)),
               "Event payload didn't match\nExpected: #{inspect(expected)} \nReceived: #{
                 inspect(actual)
               }"
      end

      defp start_event_tracers() do
        SandboxProducer
        |> Process.whereis()
        |> :erlang.trace(true, [:receive])
      end

      defp stop_event_tracers() do
        SandboxProducer
        |> Process.whereis()
        |> :erlang.trace(false, [:receive])
      end

      defp clear_mailbox() do
        receive do
          _msg ->
            clear_mailbox()
        after
          1 ->
            :ok
        end
      end
    end
  end

  defmacro assert_multiple_broadcast_event(assertions, options \\ [], do: block) do
    quote do
      start_event_tracers()

      timeout = Keyword.get(unquote(options), :timeout, @default_timeout)

      unquote(block)

      unquote(assertions)
      |> Enum.each(fn {event, event_options} ->
        assert_event_receive(event, Keyword.put(event_options, :timeout, timeout))
      end)

      stop_event_tracers()
    end
  end

  defmacro assert_broadcast_event(event, options \\ [], do: block) do
    quote do
      event = unquote(event)
      options = unquote(options)
      start_event_tracers()
      clear_mailbox()

      unquote(block)

      response = assert_event_receive(event, options)
      stop_event_tracers()

      response
    end
  end

  defmacro refute_broadcast_event(event, options \\ [], do: block) do
    quote do
      event = unquote(event)
      options = unquote(options)
      timeout = Keyword.get(options, :timeout, @default_timeout)

      start_event_tracers()

      unquote(block)

      pid = Process.whereis(SandboxProducer)

      refute_receive(
        {:trace, ^pid, :receive, {:"$gen_cast", {:publish, {:ok, %{event: ^event}}}}},
        timeout,
        "Expected event #{event} not to be broadcasted but it was"
      )

      stop_event_tracers()
    end
  end
end
