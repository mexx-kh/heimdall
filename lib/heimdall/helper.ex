defmodule Heimdall.Helper do
  require Logger

  def server_name(otp_app, base_module) do
    module_from_otp_app(otp_app, base_module)
  end

  def module_from_otp_app(otp_app, base_module) do
    name =
      otp_app
      |> Atom.to_string()
      |> String.split("_")
      |> Enum.map(&String.capitalize/1)
      |> Enum.join()

    Module.concat(base_module, name)
  end
end
