defmodule Heimdall.Event do
  @adapter Application.get_env(:heimdall, :adapter)

  def publish(payload, _) do
    @adapter.Producer.publish(payload)
    payload
  end

  def registered_events(module) do
    get_events(module.module_info(:attributes)[:events])
  end

  def normalize(event) when is_binary(event) do
    String.to_atom(event)
  end

  def normalize(event), do: event

  def get_all_receivers() do
    [Process.whereis(SandboxConsumer)]
  end

  # Internal functions

  defp get_events([event]) when event in [nil, :all], do: ["#"]
  defp get_events(events) when is_list(events), do: events
  defp get_events(_), do: []
end
