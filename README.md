# Heimdall

Event handling lib

## Dependencies

- Erlang 20.0
- Elixir 1.6

## Configuration

An entry should be added in the config file which specifies RabbitMQ credentials,
exchange and queue options.

```elixir
config :heimdall,
  adapter: Heimdall.Adapters.RabbitMQ,
  rabbitmq: System.get_env("RABBITMQ_URL") || "amqp://guest:guest@localhost",
  exchange: [name: "<exchange-name>", type: :topic],
  queue: [opts: [auto_delete: false, durable: true]],
  qos: [prefetch_count: 10]
```
